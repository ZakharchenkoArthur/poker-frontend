(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-before-auth-page-before-auth-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/before-auth-page/before-auth-page.page.html":
    /*!*********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/before-auth-page/before-auth-page.page.html ***!
      \*********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesBeforeAuthPageBeforeAuthPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div class=\"content\">\n    <img src=\"../../assets/before-auth.svg\" alt=\"\">\n    <div class=\"before-auth-content\">\n      <p class=\"main-title\">Poker</p>\n      <p class=\"second-title\">Easy to learn <br>difficult to master</p>\n      <div class=\"auth-buttons\">\n        <button type=\"button\" name=\"button\" class='sign-in-btn' (click)=\"signIn()\">Sign In</button>\n        <button type=\"button\" name=\"button\" class='sign-up-btn' (click)=\"signUp()\">Register</button>\n      </div>\n    </div>\n  </div>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/before-auth-page/before-auth-page-routing.module.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/pages/before-auth-page/before-auth-page-routing.module.ts ***!
      \***************************************************************************/

    /*! exports provided: BeforeAuthPagePageRoutingModule */

    /***/
    function srcAppPagesBeforeAuthPageBeforeAuthPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BeforeAuthPagePageRoutingModule", function () {
        return BeforeAuthPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _before_auth_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./before-auth-page.page */
      "./src/app/pages/before-auth-page/before-auth-page.page.ts");

      var routes = [{
        path: '',
        component: _before_auth_page_page__WEBPACK_IMPORTED_MODULE_3__["BeforeAuthPagePage"]
      }];

      var BeforeAuthPagePageRoutingModule = function BeforeAuthPagePageRoutingModule() {
        _classCallCheck(this, BeforeAuthPagePageRoutingModule);
      };

      BeforeAuthPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], BeforeAuthPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/before-auth-page/before-auth-page.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/before-auth-page/before-auth-page.module.ts ***!
      \*******************************************************************/

    /*! exports provided: BeforeAuthPagePageModule */

    /***/
    function srcAppPagesBeforeAuthPageBeforeAuthPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BeforeAuthPagePageModule", function () {
        return BeforeAuthPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _before_auth_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./before-auth-page-routing.module */
      "./src/app/pages/before-auth-page/before-auth-page-routing.module.ts");
      /* harmony import */


      var _before_auth_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./before-auth-page.page */
      "./src/app/pages/before-auth-page/before-auth-page.page.ts");

      var BeforeAuthPagePageModule = function BeforeAuthPagePageModule() {
        _classCallCheck(this, BeforeAuthPagePageModule);
      };

      BeforeAuthPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _before_auth_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["BeforeAuthPagePageRoutingModule"]],
        declarations: [_before_auth_page_page__WEBPACK_IMPORTED_MODULE_6__["BeforeAuthPagePage"]]
      })], BeforeAuthPagePageModule);
      /***/
    },

    /***/
    "./src/app/pages/before-auth-page/before-auth-page.page.scss":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/before-auth-page/before-auth-page.page.scss ***!
      \*******************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesBeforeAuthPageBeforeAuthPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: #fff;\n}\nion-content .content {\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  padding-bottom: 15vh;\n}\nion-content .content img {\n  width: 80%;\n  margin: 0 auto;\n  display: flex;\n}\nion-content .content .before-auth-content {\n  color: #FA563F;\n  font-size: 7vw;\n  font-weight: 700;\n  padding: 0 10vw;\n}\nion-content .content .before-auth-content .second-title {\n  color: #000;\n  font-weight: 400;\n  margin-bottom: 15vh;\n}\nion-content .content .before-auth-content .auth-buttons {\n  display: flex;\n  justify-content: center;\n}\nion-content .content .before-auth-content .auth-buttons button {\n  background: #fff;\n  color: #FA563F;\n  font-size: 2vh;\n  padding: 1vw;\n  border-style: solid;\n  border-color: #FA563F;\n  width: 40vw;\n  font-weight: 600;\n}\nion-content .content .before-auth-content .auth-buttons .sign-in-btn {\n  border-width: 2.5px 0 2.5px 2.5px;\n  border-radius: 4px 0 0 4px;\n}\nion-content .content .before-auth-content .auth-buttons .sign-up-btn {\n  border-width: 2.5px;\n  border-radius: 0 4px 4px 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYmVmb3JlLWF1dGgtcGFnZS9iZWZvcmUtYXV0aC1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0FBQ0Y7QUFDRTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSw4QkFBQTtFQUNBLG9CQUFBO0FBQ0o7QUFDSTtFQUNFLFVBQUE7RUFDQSxjQUFBO0VBQ0EsYUFBQTtBQUNOO0FBRUk7RUFDRSxjQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQUFOO0FBRU07RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQUFSO0FBR007RUFDRSxhQUFBO0VBQ0EsdUJBQUE7QUFEUjtBQUdRO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FBRFY7QUFJUTtFQUNFLGlDQUFBO0VBQ0EsMEJBQUE7QUFGVjtBQUtRO0VBQ0UsbUJBQUE7RUFDQSwwQkFBQTtBQUhWIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYmVmb3JlLWF1dGgtcGFnZS9iZWZvcmUtYXV0aC1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xuXG4gIC5jb250ZW50IHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTV2aDtcblxuICAgIGltZyB7XG4gICAgICB3aWR0aDogODAlO1xuICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgIH1cblxuICAgIC5iZWZvcmUtYXV0aC1jb250ZW50IHtcbiAgICAgIGNvbG9yOiAjRkE1NjNGO1xuICAgICAgZm9udC1zaXplOiA3dnc7XG4gICAgICBmb250LXdlaWdodDogNzAwO1xuICAgICAgcGFkZGluZzogMCAxMHZ3O1xuXG4gICAgICAuc2Vjb25kLXRpdGxlIHtcbiAgICAgICAgY29sb3I6ICMwMDA7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDE1dmg7XG4gICAgICB9XG5cbiAgICAgIC5hdXRoLWJ1dHRvbnMge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICAgICBidXR0b24ge1xuICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgICAgY29sb3I6ICNGQTU2M0Y7XG4gICAgICAgICAgZm9udC1zaXplOiAydmg7XG4gICAgICAgICAgcGFkZGluZzogMXZ3O1xuICAgICAgICAgIGJvcmRlci1zdHlsZTogc29saWQ7XG4gICAgICAgICAgYm9yZGVyLWNvbG9yOiAjRkE1NjNGO1xuICAgICAgICAgIHdpZHRoOiA0MHZ3O1xuICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIH1cblxuICAgICAgICAuc2lnbi1pbi1idG4ge1xuICAgICAgICAgIGJvcmRlci13aWR0aDogMi41cHggMCAyLjVweCAyLjVweDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiA0cHggMCAwIDRweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5zaWduLXVwLWJ0biB7XG4gICAgICAgICAgYm9yZGVyLXdpZHRoOiAyLjVweDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAwIDRweCA0cHggMDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/before-auth-page/before-auth-page.page.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/before-auth-page/before-auth-page.page.ts ***!
      \*****************************************************************/

    /*! exports provided: BeforeAuthPagePage */

    /***/
    function srcAppPagesBeforeAuthPageBeforeAuthPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BeforeAuthPagePage", function () {
        return BeforeAuthPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_native_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/screen-orientation/ngx */
      "./node_modules/@ionic-native/screen-orientation/__ivy_ngcc__/ngx/index.js");

      var BeforeAuthPagePage = /*#__PURE__*/function () {
        function BeforeAuthPagePage(router, screenOrientation) {
          _classCallCheck(this, BeforeAuthPagePage);

          this.router = router;
          this.screenOrientation = screenOrientation;
        }

        _createClass(BeforeAuthPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
          }
        }, {
          key: "signIn",
          value: function signIn() {
            this.router.navigate(['/sign-in']);
          }
        }, {
          key: "signUp",
          value: function signUp() {
            this.router.navigate(['/sign-up']);
          }
        }]);

        return BeforeAuthPagePage;
      }();

      BeforeAuthPagePage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_native_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_3__["ScreenOrientation"]
        }];
      };

      BeforeAuthPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-before-auth-page',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./before-auth-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/before-auth-page/before-auth-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./before-auth-page.page.scss */
        "./src/app/pages/before-auth-page/before-auth-page.page.scss"))["default"]]
      })], BeforeAuthPagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-before-auth-page-before-auth-page-module-es5.js.map