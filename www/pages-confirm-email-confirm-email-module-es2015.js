(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-confirm-email-confirm-email-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/confirm-email/confirm-email.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/confirm-email/confirm-email.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <form [formGroup]=\"confirmEmail\" (ngSubmit)=\"onVerificationCode(confirmEmail.value)\">\n    <div class=\"content\">\n      <div class=\"before-auth-content\">\n        <p class=\"main-title\">Poker</p>\n        <p class=\"second-title\">Check your mail and <br>and enter verification code</p>\n        <div class=\"form\">\n          <ion-input class='login-password' placeholder=\"Your verification code\" formControlName=\"token\" type=\"text\"></ion-input>\n        </div>\n        <div class=\"auth-buttons\">\n          <button type='submit' name=\"button\" class='sign-in-btn'>Confirm</button>\n        </div>\n      </div>\n    </div>\n  </form>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/confirm-email/confirm-email-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/confirm-email/confirm-email-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: ConfirmEmailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmEmailPageRoutingModule", function() { return ConfirmEmailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _confirm_email_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./confirm-email.page */ "./src/app/pages/confirm-email/confirm-email.page.ts");




const routes = [
    {
        path: '',
        component: _confirm_email_page__WEBPACK_IMPORTED_MODULE_3__["ConfirmEmailPage"]
    }
];
let ConfirmEmailPageRoutingModule = class ConfirmEmailPageRoutingModule {
};
ConfirmEmailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ConfirmEmailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/confirm-email/confirm-email.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/confirm-email/confirm-email.module.ts ***!
  \*************************************************************/
/*! exports provided: ConfirmEmailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmEmailPageModule", function() { return ConfirmEmailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _confirm_email_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./confirm-email-routing.module */ "./src/app/pages/confirm-email/confirm-email-routing.module.ts");
/* harmony import */ var _confirm_email_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./confirm-email.page */ "./src/app/pages/confirm-email/confirm-email.page.ts");







let ConfirmEmailPageModule = class ConfirmEmailPageModule {
};
ConfirmEmailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _confirm_email_routing_module__WEBPACK_IMPORTED_MODULE_5__["ConfirmEmailPageRoutingModule"]
        ],
        declarations: [_confirm_email_page__WEBPACK_IMPORTED_MODULE_6__["ConfirmEmailPage"]]
    })
], ConfirmEmailPageModule);



/***/ }),

/***/ "./src/app/pages/confirm-email/confirm-email.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/confirm-email/confirm-email.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: #fff;\n}\nion-content .content {\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  padding: 5vh 2vw;\n}\nion-content .content ion-icon {\n  width: 7vw;\n  height: 7vw;\n  color: #FA563F;\n}\nion-content .content .before-auth-content {\n  color: #FA563F;\n  font-size: 7vw;\n  font-weight: 700;\n  padding: 0 5vw;\n}\nion-content .content .before-auth-content .form {\n  margin: 4vh 0;\n  font-size: 4vw;\n}\nion-content .content .before-auth-content .form ion-input {\n  margin: 4vh 0;\n  border-bottom: 1vw #FA563F solid;\n}\nion-content .content .before-auth-content .second-title {\n  color: #000;\n  font-weight: 400;\n  margin-bottom: 4vh;\n}\nion-content .content .before-auth-content .auth-buttons {\n  display: flex;\n  justify-content: center;\n}\nion-content .content .before-auth-content .auth-buttons button {\n  background: #FA563F;\n  color: #fff;\n  font-size: 2vh;\n  padding: 3vw;\n  width: 75vw;\n  font-weight: 600;\n  border-radius: 4px;\n}\nion-content .content .sign-up-redirect {\n  color: #666;\n  font-size: 3.5vw;\n  margin: 2vw auto;\n  font-weight: 400;\n}\nion-content .content .sign-up-redirect span {\n  font-weight: 700;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29uZmlybS1lbWFpbC9jb25maXJtLWVtYWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0FBQ0Y7QUFDRTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSw4QkFBQTtFQUNBLGdCQUFBO0FBQ0o7QUFDSTtFQUNFLFVBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtBQUNOO0FBRUk7RUFDRSxjQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUFOO0FBRU07RUFDRSxhQUFBO0VBQ0EsY0FBQTtBQUFSO0FBRVE7RUFDRSxhQUFBO0VBQ0EsZ0NBQUE7QUFBVjtBQUlNO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFGUjtBQUtNO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0FBSFI7QUFLUTtFQUNFLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFIVjtBQU9JO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQUxOO0FBT007RUFDRSxnQkFBQTtBQUxSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY29uZmlybS1lbWFpbC9jb25maXJtLWVtYWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xuXG4gIC5jb250ZW50IHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBwYWRkaW5nOiA1dmggMnZ3O1xuXG4gICAgaW9uLWljb24ge1xuICAgICAgd2lkdGg6IDd2dztcbiAgICAgIGhlaWdodDogN3Z3O1xuICAgICAgY29sb3I6ICNGQTU2M0Y7XG4gICAgfVxuXG4gICAgLmJlZm9yZS1hdXRoLWNvbnRlbnQge1xuICAgICAgY29sb3I6ICNGQTU2M0Y7XG4gICAgICBmb250LXNpemU6IDd2dztcbiAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgICBwYWRkaW5nOiAwIDV2dztcblxuICAgICAgLmZvcm0ge1xuICAgICAgICBtYXJnaW46IDR2aCAwO1xuICAgICAgICBmb250LXNpemU6IDR2dztcblxuICAgICAgICBpb24taW5wdXQge1xuICAgICAgICAgIG1hcmdpbjogNHZoIDA7XG4gICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXZ3ICNGQTU2M0Ygc29saWQ7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLnNlY29uZC10aXRsZSB7XG4gICAgICAgIGNvbG9yOiAjMDAwO1xuICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiA0dmg7XG4gICAgICB9XG5cbiAgICAgIC5hdXRoLWJ1dHRvbnMge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICAgICBidXR0b24ge1xuICAgICAgICAgIGJhY2tncm91bmQ6ICNGQTU2M0Y7XG4gICAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgICAgZm9udC1zaXplOiAydmg7XG4gICAgICAgICAgcGFkZGluZzogM3Z3O1xuICAgICAgICAgIHdpZHRoOiA3NXZ3O1xuICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIC5zaWduLXVwLXJlZGlyZWN0IHtcbiAgICAgIGNvbG9yOiAjNjY2O1xuICAgICAgZm9udC1zaXplOiAzLjV2dztcbiAgICAgIG1hcmdpbjogMnZ3IGF1dG87XG4gICAgICBmb250LXdlaWdodDogNDAwO1xuXG4gICAgICBzcGFuIHtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/confirm-email/confirm-email.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/confirm-email/confirm-email.page.ts ***!
  \***********************************************************/
/*! exports provided: ConfirmEmailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmEmailPage", function() { return ConfirmEmailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_validators_validators_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/validators/validators.service */ "./src/app/services/validators/validators.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






let ConfirmEmailPage = class ConfirmEmailPage {
    constructor(router, formBuilder, validators, auth) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.validators = validators;
        this.auth = auth;
        this.submitAttempt = false;
        this.loaded = false;
    }
    ngOnInit() {
        this.loaded = false;
        this.submitAttempt = false;
        this.initForm();
        this.loaded = true;
        this.confirmEmail.valueChanges.subscribe(() => {
            this.submitAttempt = true;
        });
    }
    initForm() {
        this.confirmEmail = this.formBuilder.group({
            token: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, this.validators.validPassword])]
        }, { updateOn: 'change' });
    }
    onVerificationCode(data) {
        this.submitAttempt = true;
        let body = {
            token: data.token
        };
        this.auth.authRequest('accounts/verify-email', body).subscribe(res => {
            console.log(res);
            this.router.navigate(['/sign-in']);
        }, err => {
            console.log(err);
        });
    }
};
ConfirmEmailPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_validators_validators_service__WEBPACK_IMPORTED_MODULE_4__["ValidatorsService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }
];
ConfirmEmailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-confirm-email',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./confirm-email.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/confirm-email/confirm-email.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./confirm-email.page.scss */ "./src/app/pages/confirm-email/confirm-email.page.scss")).default]
    })
], ConfirmEmailPage);



/***/ })

}]);
//# sourceMappingURL=pages-confirm-email-confirm-email-module-es2015.js.map