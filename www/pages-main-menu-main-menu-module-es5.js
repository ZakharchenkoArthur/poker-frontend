(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-main-menu-main-menu-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main-menu/main-menu.page.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main-menu/main-menu.page.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesMainMenuMainMenuPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <ion-button class=\"play-btn\" (click)=\"goToGame()\">Play</ion-button>\n  <p>{{this.user}}</p>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/main-menu/main-menu-routing.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/main-menu/main-menu-routing.module.ts ***!
      \*************************************************************/

    /*! exports provided: MainMenuPageRoutingModule */

    /***/
    function srcAppPagesMainMenuMainMenuRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MainMenuPageRoutingModule", function () {
        return MainMenuPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _main_menu_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./main-menu.page */
      "./src/app/pages/main-menu/main-menu.page.ts");

      var routes = [{
        path: '',
        component: _main_menu_page__WEBPACK_IMPORTED_MODULE_3__["MainMenuPage"]
      }];

      var MainMenuPageRoutingModule = function MainMenuPageRoutingModule() {
        _classCallCheck(this, MainMenuPageRoutingModule);
      };

      MainMenuPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], MainMenuPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/main-menu/main-menu.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/main-menu/main-menu.module.ts ***!
      \*****************************************************/

    /*! exports provided: MainMenuPageModule */

    /***/
    function srcAppPagesMainMenuMainMenuModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MainMenuPageModule", function () {
        return MainMenuPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _main_menu_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./main-menu-routing.module */
      "./src/app/pages/main-menu/main-menu-routing.module.ts");
      /* harmony import */


      var _main_menu_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./main-menu.page */
      "./src/app/pages/main-menu/main-menu.page.ts");

      var MainMenuPageModule = function MainMenuPageModule() {
        _classCallCheck(this, MainMenuPageModule);
      };

      MainMenuPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _main_menu_routing_module__WEBPACK_IMPORTED_MODULE_5__["MainMenuPageRoutingModule"]],
        declarations: [_main_menu_page__WEBPACK_IMPORTED_MODULE_6__["MainMenuPage"]]
      })], MainMenuPageModule);
      /***/
    },

    /***/
    "./src/app/pages/main-menu/main-menu.page.scss":
    /*!*****************************************************!*\
      !*** ./src/app/pages/main-menu/main-menu.page.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesMainMenuMainMenuPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".play-btn {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFpbi1tZW51L21haW4tbWVudS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21haW4tbWVudS9tYWluLW1lbnUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBsYXktYnRuIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/main-menu/main-menu.page.ts":
    /*!***************************************************!*\
      !*** ./src/app/pages/main-menu/main-menu.page.ts ***!
      \***************************************************/

    /*! exports provided: MainMenuPage */

    /***/
    function srcAppPagesMainMenuMainMenuPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MainMenuPage", function () {
        return MainMenuPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var MainMenuPage = /*#__PURE__*/function () {
        function MainMenuPage(router) {
          _classCallCheck(this, MainMenuPage);

          this.router = router;
        }

        _createClass(MainMenuPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.user = "Welcome ".concat(localStorage.getItem('first_name'), " ").concat(localStorage.getItem('last_name'));
          }
        }, {
          key: "goToGame",
          value: function goToGame() {
            this.router.navigate(['/poker-game']);
          }
        }]);

        return MainMenuPage;
      }();

      MainMenuPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      MainMenuPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-main-menu',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./main-menu.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main-menu/main-menu.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./main-menu.page.scss */
        "./src/app/pages/main-menu/main-menu.page.scss"))["default"]]
      })], MainMenuPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-main-menu-main-menu-module-es5.js.map