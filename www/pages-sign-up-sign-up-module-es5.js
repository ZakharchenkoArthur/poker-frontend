(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-sign-up-sign-up-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-up/sign-up.page.html":
    /*!***************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-up/sign-up.page.html ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesSignUpSignUpPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <form [formGroup]=\"signUpForm\" (ngSubmit)=\"onSignUp(signUpForm.value)\">\n    <div class=\"content\">\n      <ion-icon name=\"arrow-back-outline\" (click)=\"beforeAuth()\"></ion-icon>\n      <div class=\"before-auth-content\">\n        <p class=\"main-title\">Poker</p>\n        <p class=\"second-title\">Last step and <br>Start playing!</p>\n        <div class=\"form\">\n          <ion-input class='login-email' placeholder=\"Your name\" formControlName=\"first_name\" type=\"first_name\"></ion-input>\n          <ion-input class='login-password' placeholder=\"Your surname\" formControlName=\"last_name\" type=\"last_name\"></ion-input>\n          <ion-input class='login-email' placeholder=\"Your email\" formControlName=\"email\" type=\"email\"></ion-input>\n          <ion-input class='login-password' placeholder=\"Your password\" formControlName=\"password\" type=\"password\"></ion-input>\n          <ion-input class='login-email' placeholder=\"Confirm your password\" formControlName=\"password_confirmation\" type=\"password\"></ion-input>\n        </div>\n        <div class=\"auth-buttons\">\n          <button type='submit' name=\"button\" class='sign-in-btn'>Sign In</button>\n        </div>\n      </div>\n      <p class=\"sign-up-redirect\" (click)=\"signIn()\">Do you have an account?<span>Login</span></p>\n    </div>\n  </form>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/sign-up/sign-up-routing.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/sign-up/sign-up-routing.module.ts ***!
      \*********************************************************/

    /*! exports provided: SignUpPageRoutingModule */

    /***/
    function srcAppPagesSignUpSignUpRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignUpPageRoutingModule", function () {
        return SignUpPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _sign_up_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./sign-up.page */
      "./src/app/pages/sign-up/sign-up.page.ts");

      var routes = [{
        path: '',
        component: _sign_up_page__WEBPACK_IMPORTED_MODULE_3__["SignUpPage"]
      }];

      var SignUpPageRoutingModule = function SignUpPageRoutingModule() {
        _classCallCheck(this, SignUpPageRoutingModule);
      };

      SignUpPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SignUpPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/sign-up/sign-up.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/pages/sign-up/sign-up.module.ts ***!
      \*************************************************/

    /*! exports provided: SignUpPageModule */

    /***/
    function srcAppPagesSignUpSignUpModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignUpPageModule", function () {
        return SignUpPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _sign_up_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./sign-up-routing.module */
      "./src/app/pages/sign-up/sign-up-routing.module.ts");
      /* harmony import */


      var _sign_up_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./sign-up.page */
      "./src/app/pages/sign-up/sign-up.page.ts");

      var SignUpPageModule = function SignUpPageModule() {
        _classCallCheck(this, SignUpPageModule);
      };

      SignUpPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _sign_up_routing_module__WEBPACK_IMPORTED_MODULE_5__["SignUpPageRoutingModule"]],
        declarations: [_sign_up_page__WEBPACK_IMPORTED_MODULE_6__["SignUpPage"]]
      })], SignUpPageModule);
      /***/
    },

    /***/
    "./src/app/pages/sign-up/sign-up.page.scss":
    /*!*************************************************!*\
      !*** ./src/app/pages/sign-up/sign-up.page.scss ***!
      \*************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesSignUpSignUpPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: #fff;\n}\nion-content .content {\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  padding: 5vh 2vw;\n}\nion-content .content ion-icon {\n  width: 7vw;\n  height: 7vw;\n  color: #FA563F;\n}\nion-content .content .before-auth-content {\n  color: #FA563F;\n  font-size: 7vw;\n  font-weight: 700;\n  padding: 0 5vw;\n}\nion-content .content .before-auth-content .form {\n  margin: 4vh 0;\n  font-size: 4vw;\n}\nion-content .content .before-auth-content .form ion-input {\n  margin: 4vh 0;\n  border-bottom: 1vw #FA563F solid;\n}\nion-content .content .before-auth-content .second-title {\n  color: #000;\n  font-weight: 400;\n  margin-bottom: 4vh;\n}\nion-content .content .before-auth-content .auth-buttons {\n  display: flex;\n  justify-content: center;\n}\nion-content .content .before-auth-content .auth-buttons button {\n  background: #FA563F;\n  color: #fff;\n  font-size: 2vh;\n  padding: 3vw;\n  width: 75vw;\n  font-weight: 600;\n  border-radius: 4px;\n}\nion-content .content .sign-up-redirect {\n  color: #666;\n  font-size: 3.5vw;\n  margin: 2vw auto;\n  font-weight: 400;\n}\nion-content .content .sign-up-redirect span {\n  font-weight: 700;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2lnbi11cC9zaWduLXVwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0FBQ0Y7QUFDRTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSw4QkFBQTtFQUNBLGdCQUFBO0FBQ0o7QUFDSTtFQUNFLFVBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtBQUNOO0FBRUk7RUFDRSxjQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUFOO0FBRU07RUFDRSxhQUFBO0VBQ0EsY0FBQTtBQUFSO0FBRVE7RUFDRSxhQUFBO0VBQ0EsZ0NBQUE7QUFBVjtBQUlNO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFGUjtBQUtNO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0FBSFI7QUFLUTtFQUNFLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFIVjtBQU9JO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQUxOO0FBT007RUFDRSxnQkFBQTtBQUxSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvc2lnbi11cC9zaWduLXVwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xuXG4gIC5jb250ZW50IHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBwYWRkaW5nOiA1dmggMnZ3O1xuXG4gICAgaW9uLWljb24ge1xuICAgICAgd2lkdGg6IDd2dztcbiAgICAgIGhlaWdodDogN3Z3O1xuICAgICAgY29sb3I6ICNGQTU2M0Y7XG4gICAgfVxuXG4gICAgLmJlZm9yZS1hdXRoLWNvbnRlbnQge1xuICAgICAgY29sb3I6ICNGQTU2M0Y7XG4gICAgICBmb250LXNpemU6IDd2dztcbiAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgICBwYWRkaW5nOiAwIDV2dztcblxuICAgICAgLmZvcm0ge1xuICAgICAgICBtYXJnaW46IDR2aCAwO1xuICAgICAgICBmb250LXNpemU6IDR2dztcblxuICAgICAgICBpb24taW5wdXQge1xuICAgICAgICAgIG1hcmdpbjogNHZoIDA7XG4gICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXZ3ICNGQTU2M0Ygc29saWQ7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLnNlY29uZC10aXRsZSB7XG4gICAgICAgIGNvbG9yOiAjMDAwO1xuICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiA0dmg7XG4gICAgICB9XG5cbiAgICAgIC5hdXRoLWJ1dHRvbnMge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICAgICBidXR0b24ge1xuICAgICAgICAgIGJhY2tncm91bmQ6ICNGQTU2M0Y7XG4gICAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgICAgZm9udC1zaXplOiAydmg7XG4gICAgICAgICAgcGFkZGluZzogM3Z3O1xuICAgICAgICAgIHdpZHRoOiA3NXZ3O1xuICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIC5zaWduLXVwLXJlZGlyZWN0IHtcbiAgICAgIGNvbG9yOiAjNjY2O1xuICAgICAgZm9udC1zaXplOiAzLjV2dztcbiAgICAgIG1hcmdpbjogMnZ3IGF1dG87XG4gICAgICBmb250LXdlaWdodDogNDAwO1xuXG4gICAgICBzcGFuIHtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/sign-up/sign-up.page.ts":
    /*!***********************************************!*\
      !*** ./src/app/pages/sign-up/sign-up.page.ts ***!
      \***********************************************/

    /*! exports provided: SignUpPage */

    /***/
    function srcAppPagesSignUpSignUpPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignUpPage", function () {
        return SignUpPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../services/auth/auth.service */
      "./src/app/services/auth/auth.service.ts");
      /* harmony import */


      var _services_validators_validators_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../services/validators/validators.service */
      "./src/app/services/validators/validators.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var SignUpPage = /*#__PURE__*/function () {
        function SignUpPage(router, formBuilder, validators, auth) {
          _classCallCheck(this, SignUpPage);

          this.router = router;
          this.formBuilder = formBuilder;
          this.validators = validators;
          this.auth = auth;
          this.submitAttempt = false;
          this.loaded = false;
        }

        _createClass(SignUpPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.loaded = false;
            this.submitAttempt = false;
            this.initForm();
            this.loaded = true;
            this.signUpForm.valueChanges.subscribe(function () {
              _this.submitAttempt = true;
            });
          }
        }, {
          key: "initForm",
          value: function initForm() {
            this.signUpForm = this.formBuilder.group({
              email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, this.validators.validEmail()])],
              first_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
              last_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
              password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, this.validators.validPassword])],
              password_confirmation: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
            }, {
              updateOn: 'change'
            });
          }
        }, {
          key: "onSignUp",
          value: function onSignUp(data) {
            var _this2 = this;

            this.submitAttempt = true;
            var body = {
              firstName: data.first_name,
              lastName: data.last_name,
              email: data.email,
              password: data.password,
              confirmPassword: data.password_confirmation,
              acceptTerms: true
            };
            this.auth.authRequest('accounts/register', body).subscribe(function (res) {
              _this2.router.navigate(['/confirm-email']);
            }, function (err) {
              console.log(err);
            });
          }
        }, {
          key: "signIn",
          value: function signIn() {
            this.router.navigate(['/sign-in']);
          }
        }, {
          key: "beforeAuth",
          value: function beforeAuth() {
            this.router.navigate(['/before-auth-page']);
          }
        }]);

        return SignUpPage;
      }();

      SignUpPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }, {
          type: _services_validators_validators_service__WEBPACK_IMPORTED_MODULE_4__["ValidatorsService"]
        }, {
          type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }];
      };

      SignUpPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sign-up',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./sign-up.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-up/sign-up.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./sign-up.page.scss */
        "./src/app/pages/sign-up/sign-up.page.scss"))["default"]]
      })], SignUpPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-sign-up-sign-up-module-es5.js.map