(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-poker-game-poker-game-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/poker-game/poker-game.page.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/poker-game/poker-game.page.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesPokerGamePokerGamePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div class=\"oppent-cards cards\">\n    <img class='firs-card' src=\"../../assets/poker-cards/{{showOpponentCards ? this.opponentCards[0] : 'green_back'}}.png\" alt=\"card\">\n    <img class='second-card' src=\"../../assets/poker-cards/{{showOpponentCards ? this.opponentCards[1] : 'green_back'}}.png\" alt=\"card\">\n  </div>\n  <p class='opponent-chips chips'>{{opponentChips}}$</p>\n  <p class='bank-chips chips' *ngIf=\"betsBank > 0\">{{betsBank}}$</p>\n  <p class='opponent-bets chips' *ngIf=\"opponentBet > 0\">{{opponentBet}}$</p>\n  <div class=\"table-cards\" *ngIf=\"this.tableCards\">\n    <img class='firs-card' *ngIf=\"stateGame > 0\" src=\"../../assets/poker-cards/{{this.tableCards[0]}}.png\" alt=\"card\">\n    <img class='second-card' *ngIf=\"stateGame > 0\" src=\"../../assets/poker-cards/{{this.tableCards[1]}}.png\" alt=\"card\">\n    <img class='firs-card' *ngIf=\"stateGame > 0\" src=\"../../assets/poker-cards/{{this.tableCards[2]}}.png\" alt=\"card\">\n    <img class='second-card' *ngIf=\"stateGame > 1\" src=\"../../assets/poker-cards/{{this.tableCards[3]}}.png\" alt=\"card\">\n    <img class='firs-card' *ngIf=\"stateGame > 2\" src=\"../../assets/poker-cards/{{this.tableCards[4]}}.png\" alt=\"card\">\n  </div>\n  <p class='my-bets chips' *ngIf=\"myBet > 0\">{{myBet}}$</p>\n  <p class='my-chips chips'>{{myChips}}$</p>\n  <div class=\"my-cards cards\">\n    <img class='firs-card' src=\"../../assets/poker-cards/{{this.myCards[0]}}.png\" alt=\"card\">\n    <img class='second-card' src=\"../../assets/poker-cards/{{this.myCards[1]}}.png\" alt=\"card\">\n  </div>\n  <div class=\"control-btns\">\n    <button type=\"button\" class=\"fold\" (click)=\"fold()\" name=\"button\">Fold</button>\n    <button type=\"button\" class=\"check\" (click)=\"check()\" name=\"button\">Check</button>\n    <button type=\"button\" class=\"raise\" (click)=\"raise()\" name=\"button\">Raise</button>\n  </div>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/poker-game/poker-game-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/poker-game/poker-game-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: PokerGamePageRoutingModule */

    /***/
    function srcAppPagesPokerGamePokerGameRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PokerGamePageRoutingModule", function () {
        return PokerGamePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _poker_game_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./poker-game.page */
      "./src/app/pages/poker-game/poker-game.page.ts");

      var routes = [{
        path: '',
        component: _poker_game_page__WEBPACK_IMPORTED_MODULE_3__["PokerGamePage"]
      }];

      var PokerGamePageRoutingModule = function PokerGamePageRoutingModule() {
        _classCallCheck(this, PokerGamePageRoutingModule);
      };

      PokerGamePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], PokerGamePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/poker-game/poker-game.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/poker-game/poker-game.module.ts ***!
      \*******************************************************/

    /*! exports provided: PokerGamePageModule */

    /***/
    function srcAppPagesPokerGamePokerGameModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PokerGamePageModule", function () {
        return PokerGamePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _poker_game_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./poker-game-routing.module */
      "./src/app/pages/poker-game/poker-game-routing.module.ts");
      /* harmony import */


      var _poker_game_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./poker-game.page */
      "./src/app/pages/poker-game/poker-game.page.ts");

      var PokerGamePageModule = function PokerGamePageModule() {
        _classCallCheck(this, PokerGamePageModule);
      };

      PokerGamePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _poker_game_routing_module__WEBPACK_IMPORTED_MODULE_5__["PokerGamePageRoutingModule"]],
        declarations: [_poker_game_page__WEBPACK_IMPORTED_MODULE_6__["PokerGamePage"]]
      })], PokerGamePageModule);
      /***/
    },

    /***/
    "./src/app/pages/poker-game/poker-game.page.scss":
    /*!*******************************************************!*\
      !*** ./src/app/pages/poker-game/poker-game.page.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesPokerGamePokerGamePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "img {\n  width: 20vw;\n  margin: 2vw;\n}\n\n.cards {\n  display: flex;\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%, 0);\n}\n\n.oppent-cards {\n  top: 10vh;\n}\n\n.my-cards {\n  bottom: 10vh;\n}\n\n.table-cards {\n  display: flex;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  justify-content: center;\n}\n\n.table-cards img {\n  width: 12vw;\n  margin: 2vw;\n}\n\n.chips {\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n\n.opponent-chips {\n  top: 5vh;\n}\n\n.my-chips {\n  bottom: 3vh;\n}\n\n.bank-chips {\n  top: 40vh;\n}\n\n.my-bets {\n  bottom: 30vh;\n}\n\n.opponent-bets {\n  top: 30vh;\n}\n\n.control-btns {\n  position: absolute;\n  bottom: 3vh;\n  left: 50%;\n  transform: translate(-50%, 0);\n}\n\n.control-btns button {\n  margin: 0 1vw;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcG9rZXItZ2FtZS9wb2tlci1nYW1lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7RUFDQSxXQUFBO0FBQ0Y7O0FBRUE7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsNkJBQUE7QUFDRjs7QUFFQTtFQUNFLFNBQUE7QUFDRjs7QUFFQTtFQUNFLFlBQUE7QUFDRjs7QUFFQTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7RUFDQSx1QkFBQTtBQUNGOztBQUNFO0VBQ0UsV0FBQTtFQUNBLFdBQUE7QUFDSjs7QUFHQTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0FBQUY7O0FBR0E7RUFDRSxRQUFBO0FBQUY7O0FBR0E7RUFDRSxXQUFBO0FBQUY7O0FBR0E7RUFDRSxTQUFBO0FBQUY7O0FBR0E7RUFDRSxZQUFBO0FBQUY7O0FBR0E7RUFDRSxTQUFBO0FBQUY7O0FBR0E7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsNkJBQUE7QUFBRjs7QUFFRTtFQUNFLGFBQUE7QUFBSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Bva2VyLWdhbWUvcG9rZXItZ2FtZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpbWcge1xuICB3aWR0aDogMjB2dztcbiAgbWFyZ2luOiAydnc7XG59XG5cbi5jYXJkcyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLDApO1xufVxuXG4ub3BwZW50LWNhcmRzIHtcbiAgdG9wOiAxMHZoO1xufVxuXG4ubXktY2FyZHMge1xuICBib3R0b206IDEwdmg7XG59XG5cbi50YWJsZS1jYXJkcyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwtNTAlKTtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cbiAgaW1nIHtcbiAgICB3aWR0aDogMTJ2dztcbiAgICBtYXJnaW46IDJ2dztcbiAgfVxufVxuXG4uY2hpcHMge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG59XG5cbi5vcHBvbmVudC1jaGlwcyB7XG4gIHRvcDogNXZoO1xufVxuXG4ubXktY2hpcHMge1xuICBib3R0b206IDN2aDtcbn1cblxuLmJhbmstY2hpcHMge1xuICB0b3A6IDQwdmg7XG59XG5cbi5teS1iZXRzIHtcbiAgYm90dG9tOiAzMHZoO1xufVxuXG4ub3Bwb25lbnQtYmV0cyB7XG4gIHRvcDogMzB2aDtcbn1cblxuLmNvbnRyb2wtYnRucyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAzdmg7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgMCk7XG5cbiAgYnV0dG9uIHtcbiAgICBtYXJnaW46IDAgMXZ3O1xuICB9XG59XG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/poker-game/poker-game.page.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/poker-game/poker-game.page.ts ***!
      \*****************************************************/

    /*! exports provided: PokerGamePage */

    /***/
    function srcAppPagesPokerGamePokerGamePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PokerGamePage", function () {
        return PokerGamePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _services_requests_requests_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../services/requests/requests.service */
      "./src/app/services/requests/requests.service.ts");

      var PokerGamePage = /*#__PURE__*/function () {
        function PokerGamePage(request) {
          _classCallCheck(this, PokerGamePage);

          this.request = request;
          this.pokerCards = ['2C', '2D', '2H', '2S', '3C', '3D', '3H', '3S', '4C', '4D', '4H', '4S', '5C', '5D', '5H', '5S', '6C', '6D', '6H', '6S', '7C', '7D', '7H', '7S', '8C', '8D', '8H', '8S', '9C', '9D', '9H', '9S', '10C', '10D', '10H', '10S', 'JC', 'JD', 'JH', 'JS', 'QC', 'QD', 'QH', 'QS', 'KC', 'KD', 'KH', 'KS', 'AC', 'AD', 'AH', 'AS'];
          this.showOpponentCards = false;
          this.opponentChips = 500;
          this.myChips = 500;
          this.pokerButton = 'my';
          this.stateGame = 0;
          this.betsBank = 0;
          this.opponentBet = 0;
          this.myBet = 0;
          this.smallBlind = 10;
          this.matchCount = 0;
        }

        _createClass(PokerGamePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.matchPokerCards = this.shuffle(this.pokerCards);
            this.getOpponentCards();
            this.getMyCards();
            this.startMatch();
          }
        }, {
          key: "getOpponentCards",
          value: function getOpponentCards() {
            var card;
            this.opponentCards = [];

            for (var index = 0; index < 2; index++) {
              card = this.getRamdomCardFromDeck(this.matchPokerCards);
              this.opponentCards.push(card);
            }
          }
        }, {
          key: "getMyCards",
          value: function getMyCards() {
            var card;
            this.myCards = [];

            for (var index = 0; index < 2; index++) {
              card = this.getRamdomCardFromDeck(this.matchPokerCards);
              this.myCards.push(card);
            }

            this.play();
          }
        }, {
          key: "getRamdomCardFromDeck",
          value: function getRamdomCardFromDeck(array) {
            var randomCard = array[Math.floor(Math.random() * array.length)];
            array.splice(array.indexOf(randomCard), 1);
            return randomCard;
          }
        }, {
          key: "shuffle",
          value: function shuffle(array) {
            var m = array.length,
                t,
                i;

            while (m > 0) {
              i = Math.floor(Math.random() * m--);
              t = array[m];
              array[m] = array[i];
              array[i] = t;
            }

            return array;
          }
        }, {
          key: "play",
          value: function play() {
            var card;
            this.showOpponentCards = true;
            this.tableCards = [];

            for (var index = 0; index < 5; index++) {
              card = this.getRamdomCardFromDeck(this.matchPokerCards);
              this.tableCards.push(card);
            }
          }
        }, {
          key: "knowWinner",
          value: function knowWinner(myCards, opponentCards, tableCards) {
            var winners = [];
            this.request.getGameResult("cc=".concat(tableCards.join(','), "&pc[]=").concat(myCards.join(','), "&pc[]=").concat(opponentCards.join(','))).subscribe(function (res) {
              res.winners.forEach(function (element) {
                if (element.cards == myCards.join(',')) {
                  winners.push({
                    user: 'you',
                    result: element.result
                  });
                } else if (element.cards == opponentCards.join(',')) {
                  winners.push({
                    user: 'opponent',
                    result: element.result
                  });
                }
              });

              if (winners.length == 1) {
                if (winners[0].user == 'you') {
                  console.log("You win");
                  console.log("You win with ".concat(winners[0].result));
                } else if (winners[0].user == 'opponent') {
                  console.log("You lose");
                  console.log("Opponent win with ".concat(winners[0].result));
                }
              } else {
                console.log('draw');
              }

              winners;
            }, function (err) {
              console.log(err);
            });
          }
        }, {
          key: "fold",
          value: function fold() {
            this.opponentChips += this.betsBank;
            this.matchCount++;
            this.stateGame = 0;
            this.betsBank = 0;
            this.pokerButton == 'my' ? this.pokerButton = 'opponent' : this.pokerButton = 'my';
            this.startMatch();
          }
        }, {
          key: "check",
          value: function check() {
            this.myChips -= this.opponentBet - this.myBet;
            this.myBet = this.opponentBet;
            this.stateGame += 1;
            this.betsBank = this.myBet + this.opponentBet;
            this.myBet = 0;
            this.opponentBet = 0;

            if (this.stateGame == 3) {
              this.knowWinner(this.myCards, this.opponentCards, this.tableCards);
            }
          }
        }, {
          key: "raise",
          value: function raise() {}
        }, {
          key: "startMatch",
          value: function startMatch() {
            if (this.pokerButton == 'my') {
              this.myChips -= this.smallBlind;
              this.myBet = this.smallBlind;
              this.opponentChips -= 2 * this.smallBlind;
              this.opponentBet = 2 * this.smallBlind;
            } else if (this.pokerButton == 'opponent') {
              this.opponentChips -= this.smallBlind;
              this.opponentBet = this.smallBlind;
              this.myChips -= 2 * this.smallBlind;
              this.myBet = 2 * this.smallBlind;
            }
          }
        }]);

        return PokerGamePage;
      }();

      PokerGamePage.ctorParameters = function () {
        return [{
          type: _services_requests_requests_service__WEBPACK_IMPORTED_MODULE_2__["RequestsService"]
        }];
      };

      PokerGamePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-poker-game',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./poker-game.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/poker-game/poker-game.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./poker-game.page.scss */
        "./src/app/pages/poker-game/poker-game.page.scss"))["default"]]
      })], PokerGamePage);
      /***/
    },

    /***/
    "./src/app/services/requests/requests.service.ts":
    /*!*******************************************************!*\
      !*** ./src/app/services/requests/requests.service.ts ***!
      \*******************************************************/

    /*! exports provided: RequestsService */

    /***/
    function srcAppServicesRequestsRequestsServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RequestsService", function () {
        return RequestsService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var RequestsService = /*#__PURE__*/function () {
        function RequestsService(http) {
          _classCallCheck(this, RequestsService);

          this.http = http;
          this.url = 'https://poker-game-back.herokuapp.com/';
          this.httpOptions = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('auth_token')
          });
        }

        _createClass(RequestsService, [{
          key: "getGameResult",
          value: function getGameResult(path) {
            return this.http.get('https://api.pokerapi.dev/v1/winner/texas_holdem?' + path);
          }
        }]);

        return RequestsService;
      }();

      RequestsService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      RequestsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], RequestsService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-poker-game-poker-game-module-es5.js.map