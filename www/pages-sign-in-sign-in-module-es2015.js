(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-sign-in-sign-in-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-in/sign-in.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-in/sign-in.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <form [formGroup]=\"signinForm\" (ngSubmit)=\"onSignIn(signinForm.value)\">\n    <div class=\"content\">\n      <ion-icon name=\"arrow-back-outline\" (click)=\"beforeAuth()\"></ion-icon>\n      <div class=\"before-auth-content\">\n        <p class=\"main-title\">Poker</p>\n        <p class=\"second-title\">Hello there!<br>Welcome Back</p>\n        <div class=\"form\">\n          <ion-input class='login-email' placeholder=\"Your email\" formControlName=\"email\" type=\"email\"></ion-input>\n          <ion-input class='login-password' placeholder=\"Your password\" formControlName=\"password\" type=\"password\"></ion-input>\n        </div>\n        <div class=\"auth-buttons\">\n          <button type='submit' name=\"button\" class='sign-in-btn'>Sign In</button>\n        </div>\n      </div>\n      <p class=\"sign-up-redirect\" (click)=\"signUp()\">Don't have an account?<span>Register</span></p>\n    </div>\n  </form>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/sign-in/sign-in-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/sign-in/sign-in-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: SignInPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignInPageRoutingModule", function() { return SignInPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _sign_in_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sign-in.page */ "./src/app/pages/sign-in/sign-in.page.ts");




const routes = [
    {
        path: '',
        component: _sign_in_page__WEBPACK_IMPORTED_MODULE_3__["SignInPage"]
    }
];
let SignInPageRoutingModule = class SignInPageRoutingModule {
};
SignInPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SignInPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/sign-in/sign-in.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/sign-in/sign-in.module.ts ***!
  \*************************************************/
/*! exports provided: SignInPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignInPageModule", function() { return SignInPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _sign_in_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sign-in-routing.module */ "./src/app/pages/sign-in/sign-in-routing.module.ts");
/* harmony import */ var _sign_in_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sign-in.page */ "./src/app/pages/sign-in/sign-in.page.ts");







let SignInPageModule = class SignInPageModule {
};
SignInPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _sign_in_routing_module__WEBPACK_IMPORTED_MODULE_5__["SignInPageRoutingModule"]
        ],
        declarations: [_sign_in_page__WEBPACK_IMPORTED_MODULE_6__["SignInPage"]]
    })
], SignInPageModule);



/***/ }),

/***/ "./src/app/pages/sign-in/sign-in.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/sign-in/sign-in.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: #fff;\n}\nion-content .content {\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  padding: 5vh 2vw;\n}\nion-content .content ion-icon {\n  width: 7vw;\n  height: 7vw;\n  color: #FA563F;\n}\nion-content .content .before-auth-content {\n  color: #FA563F;\n  font-size: 7vw;\n  font-weight: 700;\n  padding: 0 5vw;\n}\nion-content .content .before-auth-content .form {\n  margin: 20vh 0;\n  font-size: 4vw;\n}\nion-content .content .before-auth-content .form ion-input {\n  margin: 4vh 0;\n  border-bottom: 1vw #FA563F solid;\n}\nion-content .content .before-auth-content .second-title {\n  color: #000;\n  font-weight: 400;\n  margin-bottom: 15vh;\n}\nion-content .content .before-auth-content .auth-buttons {\n  display: flex;\n  justify-content: center;\n}\nion-content .content .before-auth-content .auth-buttons button {\n  background: #FA563F;\n  color: #fff;\n  font-size: 2vh;\n  padding: 3vw;\n  width: 75vw;\n  font-weight: 600;\n  border-radius: 4px;\n}\nion-content .content .sign-up-redirect {\n  color: #666;\n  font-size: 3.5vw;\n  margin: 2vw auto;\n  font-weight: 400;\n}\nion-content .content .sign-up-redirect span {\n  font-weight: 700;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2lnbi1pbi9zaWduLWluLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0FBQ0Y7QUFDRTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSw4QkFBQTtFQUNBLGdCQUFBO0FBQ0o7QUFDSTtFQUNFLFVBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtBQUNOO0FBRUk7RUFDRSxjQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUFOO0FBRU07RUFDRSxjQUFBO0VBQ0EsY0FBQTtBQUFSO0FBRVE7RUFDRSxhQUFBO0VBQ0EsZ0NBQUE7QUFBVjtBQUlNO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFGUjtBQUtNO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0FBSFI7QUFLUTtFQUNFLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFIVjtBQU9JO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQUxOO0FBT007RUFDRSxnQkFBQTtBQUxSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvc2lnbi1pbi9zaWduLWluLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xuXG4gIC5jb250ZW50IHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBwYWRkaW5nOiA1dmggMnZ3O1xuXG4gICAgaW9uLWljb24ge1xuICAgICAgd2lkdGg6IDd2dztcbiAgICAgIGhlaWdodDogN3Z3O1xuICAgICAgY29sb3I6ICNGQTU2M0Y7XG4gICAgfVxuXG4gICAgLmJlZm9yZS1hdXRoLWNvbnRlbnQge1xuICAgICAgY29sb3I6ICNGQTU2M0Y7XG4gICAgICBmb250LXNpemU6IDd2dztcbiAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgICBwYWRkaW5nOiAwIDV2dztcblxuICAgICAgLmZvcm0ge1xuICAgICAgICBtYXJnaW46IDIwdmggMDtcbiAgICAgICAgZm9udC1zaXplOiA0dnc7XG5cbiAgICAgICAgaW9uLWlucHV0IHtcbiAgICAgICAgICBtYXJnaW46IDR2aCAwO1xuICAgICAgICAgIGJvcmRlci1ib3R0b206IDF2dyAjRkE1NjNGIHNvbGlkO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC5zZWNvbmQtdGl0bGUge1xuICAgICAgICBjb2xvcjogIzAwMDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTV2aDtcbiAgICAgIH1cblxuICAgICAgLmF1dGgtYnV0dG9ucyB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgICAgIGJ1dHRvbiB7XG4gICAgICAgICAgYmFja2dyb3VuZDogI0ZBNTYzRjtcbiAgICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgICBmb250LXNpemU6IDJ2aDtcbiAgICAgICAgICBwYWRkaW5nOiAzdnc7XG4gICAgICAgICAgd2lkdGg6IDc1dnc7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgLnNpZ24tdXAtcmVkaXJlY3Qge1xuICAgICAgY29sb3I6ICM2NjY7XG4gICAgICBmb250LXNpemU6IDMuNXZ3O1xuICAgICAgbWFyZ2luOiAydncgYXV0bztcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG5cbiAgICAgIHNwYW4ge1xuICAgICAgICBmb250LXdlaWdodDogNzAwO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuIl19 */");

/***/ }),

/***/ "./src/app/pages/sign-in/sign-in.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/sign-in/sign-in.page.ts ***!
  \***********************************************/
/*! exports provided: SignInPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignInPage", function() { return SignInPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_validators_validators_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/validators/validators.service */ "./src/app/services/validators/validators.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






let SignInPage = class SignInPage {
    constructor(router, formBuilder, validators, auth) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.validators = validators;
        this.auth = auth;
        this.submitAttempt = false;
        this.loaded = false;
    }
    ngOnInit() {
        this.loaded = false;
        this.submitAttempt = false;
        this.initForm();
        this.loaded = true;
        this.signinForm.valueChanges.subscribe(() => {
            this.submitAttempt = true;
        });
    }
    initForm() {
        this.signinForm = this.formBuilder.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, this.validators.validEmail()])],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, this.validators.validPassword])]
        }, { updateOn: 'change' });
    }
    onSignIn(data) {
        this.submitAttempt = true;
        let body = {
            email: data.email,
            password: data.password
        };
        localStorage.clear();
        this.auth.authRequest('accounts/authenticate', body).subscribe(res => {
            console.log(res.body);
            localStorage.setItem('auth_token', res.body.jwtToken);
            localStorage.setItem('first_name', res.body.firstName);
            localStorage.setItem('last_name', res.body.lastName);
            this.router.navigate(['/main-menu']);
        }, err => {
            console.log(err);
        });
    }
    signUp() {
        this.router.navigate(['/sign-up']);
    }
    beforeAuth() {
        this.router.navigate(['/before-auth-page']);
    }
};
SignInPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_validators_validators_service__WEBPACK_IMPORTED_MODULE_4__["ValidatorsService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }
];
SignInPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sign-in',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./sign-in.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-in/sign-in.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./sign-in.page.scss */ "./src/app/pages/sign-in/sign-in.page.scss")).default]
    })
], SignInPage);



/***/ })

}]);
//# sourceMappingURL=pages-sign-in-sign-in-module-es2015.js.map