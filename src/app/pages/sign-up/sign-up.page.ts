import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { ValidatorsService } from '../../services/validators/validators.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

  public signUpForm: FormGroup;
  submitAttempt: boolean = false;
  loaded = false;

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public validators: ValidatorsService,
    public auth: AuthService
  ) { }

  ngOnInit() {
    this.loaded = false;
    this.submitAttempt = false;
    this.initForm();
    this.loaded = true;
    this.signUpForm.valueChanges.subscribe(()=>{
      this.submitAttempt = true;
    });
  }

  initForm() {
    this.signUpForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, this.validators.validEmail()])],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, this.validators.validPassword])],
      password_confirmation: ['', Validators.required]
    } , { updateOn: 'change' });
  }


  onSignUp(data) {
    this.submitAttempt = true;
    let body = {
      firstName: data.first_name,
      lastName: data.last_name,
      email: data.email,
      password: data.password,
      confirmPassword: data.password_confirmation,
      acceptTerms: true
    }
    this.auth.authRequest('accounts/register', body).subscribe(
      res => {
        this.router.navigate(['/confirm-email'])
      },
      err => {
        console.log(err)
      }
    )
  }

  signIn() {
    this.router.navigate(['/sign-in'])
  }

  beforeAuth() {
    this.router.navigate(['/before-auth-page'])
  }
}
