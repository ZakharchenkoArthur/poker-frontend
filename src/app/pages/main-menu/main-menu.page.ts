import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Socket } from 'ngx-socket-io';
import { SocketService } from '../../services/socket/socket.service'

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.page.html',
  styleUrls: ['./main-menu.page.scss'],
})
export class MainMenuPage implements OnInit {

  socketEvents = this.socket.fromEvent<string[]>('prepareMatch');
  private socketSub: Subscription;

  constructor(
    private router: Router,
    private socket: Socket,
    public socketEmits: SocketService
  ) {
    this.socketSub = this.socketEvents.subscribe(body => {
        if (!body['error']) {
          this.goToGame();
        } else {
          console.log(body['error']);
        }
      }
    );
  }

  user:string;

  ngOnInit() {
    this.user = `Welcome ${localStorage.getItem('first_name')} ${localStorage.getItem('last_name')}`
  }

  goToGame() {
    this.router.navigate(['/poker-game'])
  }
}
