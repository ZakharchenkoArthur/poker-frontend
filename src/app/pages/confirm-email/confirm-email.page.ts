import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { ValidatorsService } from '../../services/validators/validators.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.page.html',
  styleUrls: ['./confirm-email.page.scss'],
})
export class ConfirmEmailPage implements OnInit {

  public confirmEmail: FormGroup;
  submitAttempt: boolean = false;
  loaded = false;

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public validators: ValidatorsService,
    public auth: AuthService
  ) { }

  ngOnInit() {
    this.loaded = false;
    this.submitAttempt = false;
    this.initForm();
    this.loaded = true;
    this.confirmEmail.valueChanges.subscribe(()=>{
      this.submitAttempt = true;
    });
  }

  initForm() {
    this.confirmEmail = this.formBuilder.group({
      token: ['', Validators.compose([Validators.required, this.validators.validPassword])]
    } , { updateOn: 'change' });
  }


  onVerificationCode(data) {
    this.submitAttempt = true;
    let body = {
      token: data.token
    }
    this.auth.authRequest('accounts/verify-email', body).subscribe(
      res => {
        console.log(res)
        this.router.navigate(['/sign-in'])
      },
      err => {
        console.log(err)
      }
    )
  }
}
