import { Component, OnInit } from '@angular/core';
import { RequestsService } from '../../services/requests/requests.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Socket } from 'ngx-socket-io';
import { SocketService } from '../../services/socket/socket.service'
import { NgCircleProgressModule } from 'ng-circle-progress';
import { Options } from '@angular-slider/ngx-slider';

interface SimpleSliderModel {
  value: number;
  options: Options;
}

@Component({
  selector: 'app-poker-game',
  templateUrl: './poker-game.page.html',
  styleUrls: ['./poker-game.page.scss'],
})
export class PokerGamePage implements OnInit {

  verticalSlider = {
    value: 0,
    options: {
      floor: 0,
      ceil: 500,
      step: 5,
      vertical: true,
      showSelectionBar: true
    }
  };

  socketEvents = this.socket.fromEvent<any>('gameEvents');
  private socketSub: Subscription;
  opponentChips = 0;
  myChips = 0;
  betsBank = 0;
  opponentBet = 0;
  myBet = 0;
  myCards = [];
  tableCards = [];
  opponentCards = [];
  user: string;
  time: number = 10000;
  color: string;
  turn: boolean = false;
  timer: any;

  constructor(
    public request: RequestsService,
    private router: Router,
    private socket: Socket,
    public socketEmits: SocketService
  ) {
    this.user = `${localStorage.getItem('first_name')} ${localStorage.getItem('last_name')}`
  }

  ngOnInit() {
    this.socketSub = this.socketEvents.subscribe(body => {
      if (!body.error || !body.message) {
        console.log(body)
        this.tableCards = body.showedTableCards;
        this.myCards = body[body.user_type].cards;
        this.myBet = body[body.user_type].bet;
        this.opponentBet = body.user_type == 'firstPlayer' ? body['secondPlayer'].bet : body['firstPlayer'].bet;
        this.myChips = body[body.user_type].bank;
        this.verticalSlider.options = { floor: 0, ceil: Number(body[body.user_type].bank), step: 5, vertical: true, showSelectionBar: true};
        this.opponentChips = body.user_type == 'firstPlayer' ? body['secondPlayer'].bank : body['firstPlayer'].bank;
        this.betsBank = body.betsBank;
        this.turn = body.turn == localStorage.getItem('my_id') ? true : false;
        if (this.turn) {
          this.timer = setInterval(()=>{
            if (this.time == 0) {
              clearInterval(this.timer);
              this.socketEmits.fold();
              this.time = 10000;
            } else {
              this.time-=5
            }
          },10)
        } else {
          clearInterval(this.timer);
          this.time = 10000;
        }
        if (body.winner.length == 1) {
          if (body.winner[0].cards == this.myCards.join()) {
            console.log(`You win with ${body.winner[0].result}`)
          } else {
            this.opponentCards = body.winner[0].cards.split(',');
            console.log(`Opponent win with ${body.winner[0].result}`)
          }
        } else if (body.winner.length == 2) {
          alert("ничия")
        }
        if (body.gameWinner && body.gameWinner == localStorage.getItem('my_id')) {
          alert('You win')
        } else if (body.gameWinner) {
          alert('You lose')
        }
      }
    });
  }

  turnTime() {
    return this.time/100
  }

  checkColor() {
    if (this.time >= 75) {
      return '#00FF00'
    } else if (75 > this.time && 25 < this.time) {
      return '#FF8C00'
    } else if (this.time <= 25) {
      return '#DC143C'
    }
  }

}
