import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PokerGamePageRoutingModule } from './poker-game-routing.module';

import { PokerGamePage } from './poker-game.page';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PokerGamePageRoutingModule,
    NgxSliderModule,
    NgCircleProgressModule.forRoot({
      "radius": 60,
      "space": -5,
      "outerStrokeWidth": 5,
      "outerStrokeColor": "#76C2AF",
      "innerStrokeColor": "#ffffff",
      "innerStrokeWidth": 5,
      "imageSrc": "assets/icon/avatar.png",
      "imageHeight": 105,
      "imageWidth": 105,
      "showImage": true,
      "showBackground": false})
  ],
  declarations: [PokerGamePage]
})
export class PokerGamePageModule {}
