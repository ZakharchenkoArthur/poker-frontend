import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PokerGamePage } from './poker-game.page';

describe('PokerGamePage', () => {
  let component: PokerGamePage;
  let fixture: ComponentFixture<PokerGamePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokerGamePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PokerGamePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
