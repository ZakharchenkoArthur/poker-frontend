import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BeforeAuthPagePageRoutingModule } from './before-auth-page-routing.module';

import { BeforeAuthPagePage } from './before-auth-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BeforeAuthPagePageRoutingModule
  ],
  declarations: [BeforeAuthPagePage]
})
export class BeforeAuthPagePageModule {}
