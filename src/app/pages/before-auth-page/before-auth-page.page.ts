import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-before-auth-page',
  templateUrl: './before-auth-page.page.html',
  styleUrls: ['./before-auth-page.page.scss'],
})
export class BeforeAuthPagePage implements OnInit {

  constructor(
    private router: Router,
    private screenOrientation: ScreenOrientation,
  ) { }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  signIn() {
    this.router.navigate(['/sign-in'])
  }

  signUp() {
    this.router.navigate(['/sign-up'])
  }

}
