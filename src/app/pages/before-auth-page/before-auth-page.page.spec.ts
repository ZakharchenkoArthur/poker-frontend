import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BeforeAuthPagePage } from './before-auth-page.page';

describe('BeforeAuthPagePage', () => {
  let component: BeforeAuthPagePage;
  let fixture: ComponentFixture<BeforeAuthPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeforeAuthPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BeforeAuthPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
