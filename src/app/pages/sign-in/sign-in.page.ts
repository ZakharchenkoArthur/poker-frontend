import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { ValidatorsService } from '../../services/validators/validators.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage implements OnInit {

  public signinForm: FormGroup;
  submitAttempt: boolean = false;
  loaded = false;

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public validators: ValidatorsService,
    public auth: AuthService
  ) { }

  ngOnInit() {
    this.loaded = false;
    this.submitAttempt = false;
    this.initForm();
    this.loaded = true;
    this.signinForm.valueChanges.subscribe(()=>{
      this.submitAttempt = true;
    });
  }

  initForm() {
    this.signinForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, this.validators.validEmail()])],
      password: ['', Validators.compose([Validators.required, this.validators.validPassword])]
    } , { updateOn: 'change' });
  }


  onSignIn(data) {
    this.submitAttempt = true;
    let body = {
      email: data.email,
      password: data.password
    }
    localStorage.clear();
    this.auth.authRequest('accounts/authenticate', body).subscribe(
      res => {
        localStorage.setItem('my_id',res.body.id);
        localStorage.setItem('auth_token',res.body.jwtToken);
        localStorage.setItem('first_name',res.body.firstName)
        localStorage.setItem('last_name',res.body.lastName)
        this.router.navigate(['/main-menu'])
      },
      err => {
        console.log(err)
      }
    )
  }

  signUp() {
    this.router.navigate(['/sign-up'])
  }

  beforeAuth() {
    this.router.navigate(['/before-auth-page'])
  }
}
