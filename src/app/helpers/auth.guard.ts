import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    const userToken = localStorage.getItem('auth_token');
    if (userToken) {
      return true
    } else {
      this.router.navigate(['/before-auth-page']).then(() => {
        localStorage.clear();
      });
      return false;
    }
  }
}
