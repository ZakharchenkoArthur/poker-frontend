import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidatorsService {

  constructor() { }

  public validPassword(control: FormControl) {
    if (control.value.length < 6){
      return ({restrictedText: 'The password must be at least 6 characters.'})
    } else if (!control.value.match(/[a-z]/g)) {
      return ({restrictedText: 'The password must be at least one lowercase letter.'})
    // } else if (!control.value.match(/[A-Z]/g)) {
    //   return ({restrictedText: 'The password must be at least one uppercase letter.'})
    // } else if (!control.value.match(/[!@#$%^&*]/g)) {
    //   return ({restrictedText: 'The password must be at least one of the special character (!@#$%^&*).'})
    } else if (!control.value.match(/[0-9]/g)) {
      return ({restrictedText: 'The password must be at least one of the number (0–9).'})
    } else {
      return null
    }
  }

  public validEmail() {
    return function (control: FormControl) {
      let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (re.test(control.value)) {
        return null
      } else {
        return ({restrictedText: 'Email is invalid.'})
      }
    }
  }

  public validConfirmPassword(new_password) {
    return function (control: FormControl) {
      const controlToCompare = control.parent.get('password');
      if (controlToCompare && controlToCompare.value == control.value) {
        return null
      } else {
        return ({restrictedText: 'The passwords do not match.'})
      }
    }
  }

  public validConfirmEmail(new_email) {
    return function (control: FormControl) {
      if (new_email == control.value) {
        return null
      } else {
        return ({restrictedText: 'The emails do not match.'})
      }
    }
  }

  public validationError(errors) {
    let errorMessages = '';
    if (errors) {
      for (let key of Object.keys(errors)) {
        if (key == 'required') {
          errorMessages += 'This field is required. ';
        } else {
          if (errors[key].constructor === Array) {
            errorMessages += errors[key][0]
          } else {
            errorMessages += errors[key];
          }
        }
      }
    }
    return errorMessages;
  }
}
