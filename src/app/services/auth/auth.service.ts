import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url: string = 'https://poker-game-back.herokuapp.com/';

  isLoggedIn = false;
  token:any;

  constructor(
    public http: HttpClient
  ) { }

  setHeaders() {
  const token = localStorage.getItem('auth_user');

  if (token) {
    return new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token
    });
  }
}

  authRequest(path, body): Observable<any> {
    console.log(body)
    return this.http.post(`${this.url}` + path, body, {
      headers: this.setHeaders(),
      observe: 'response'
    });
  }
}
