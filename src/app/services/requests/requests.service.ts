import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  url: string = 'https://poker-game-back.herokuapp.com/';

  httpOptions: any;
  token: any;

  constructor(
    public http: HttpClient
  ) {
    this.httpOptions = new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('auth_token')
    });
  }

  getGameResult(path) {
    return this.http.get<any>('https://api.pokerapi.dev/v1/winner/texas_holdem?' + path);
  }
}
