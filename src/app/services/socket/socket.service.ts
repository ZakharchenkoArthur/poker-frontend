import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  roomId: any;

  constructor(private socket: Socket) { }

  join() {
    let id = (<HTMLInputElement>document.getElementById('inviteCode')).value;
    this.roomId = id;
    this.socket.emit('joinGame', {room_id: id, user_id: localStorage.getItem('my_id'), user_name: localStorage.getItem('first_name')});
  }

  create() {
    let id = (<HTMLInputElement>document.getElementById('inviteCode')).value;
    this.roomId = id;
    this.socket.emit('createGame', {room_id: id, user_id: localStorage.getItem('my_id'), user_name: localStorage.getItem('first_name')});
  }

  check() {
    this.socket.emit('check_call', {room_id: this.roomId, user_id: localStorage.getItem('my_id')});
  }

  fold() {
    this.socket.emit('fold', {room_id: this.roomId, user_id: localStorage.getItem('my_id')});
  }

  raise(raiseBet) {
    this.socket.emit('raise', {room_id: this.roomId, user_id: localStorage.getItem('my_id'), raiseBet: Number(raiseBet)});
  }

}
